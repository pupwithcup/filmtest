import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Observable, throwError, forkJoin } from 'rxjs';
import { ShortFilm, Film } from './omdb';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { SearchOmdbResponse } from './omdb';

@Injectable({
  providedIn: 'root'
})
export class OmdbService {

  constructor(private http: HttpClient) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(error.error.message);
    } else {
      console.error(error.status, error.error);
    }
    return throwError('error');
  }

  searchFilms(term: string, pages: number): Observable<ShortFilm[]> {
    let options = {
      params: new HttpParams()
        .set('apiKey', environment.omdbApi.key)
        .set('s', term)
    };
    let pages$ = [];
    for (let i = 1; i <= pages; i++) {
      options.params = options.params.set('page', String(i));
      pages$.push(this.http.get<SearchOmdbResponse>(environment.omdbApi.url, options).pipe(
        map((response: SearchOmdbResponse) => {
          if (response.Response === 'True') {
            return response.Search;
          } else {
            console.log(response.Error);
            return [];
          }
        }),
        catchError(this.handleError)
      ));
    }
    return forkJoin(pages$).pipe(map((combined: ShortFilm[][]) => {
      return combined.flat();
    }));
  }

  getFilm(imdbID: string): Observable<Film> {
    let options = {
      params: new HttpParams()
        .set('apiKey', environment.omdbApi.key)
        .set('i', imdbID)
    };
    return this.http.get<Film>(environment.omdbApi.url, options).pipe(
      map((response: Film) => {
        if (response.Response !== 'True') {
          console.log(response.Error);
        }
        return response;
      }),
      catchError(this.handleError)
    );
  }

}
