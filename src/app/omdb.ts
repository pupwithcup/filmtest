export class SearchOmdbResponse {
  Response: string;
  Search?: ShortFilm[];
  totalResults?: string;
  Error?: string;
}

export class ShortFilm {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}

export class Film {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;

  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Actors: string;
  Plot: string;
  imdbRating: string;

  Response: string;
  Error?: string;
}
