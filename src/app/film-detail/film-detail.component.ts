import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Film } from '../omdb';
import { OmdbService } from '../omdb.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { LocalService } from '../local-storage.service';
import { findLast } from '@angular/compiler/src/directive_resolver';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.less']
})
export class FilmDetailComponent implements OnInit {

  public film: Film;

  constructor(
    private omdbService: OmdbService,
    private route: ActivatedRoute,
    private localService: LocalService
  ) { }

  ngOnInit() {
    this.route.params.pipe(switchMap(routeParams => {
      return this.omdbService.getFilm(routeParams.imdbID);
    })).subscribe((film: Film) => {
      this.film = film;
      this.localService.updateHistory({
        Title: film.Title,
        Year: film.Year,
        imdbID: film.imdbID,
        Type: film.Type,
        Poster: film.Poster
      });
    });
  }

}
