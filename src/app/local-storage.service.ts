import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { ShortFilm } from './omdb';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  public history$ = new Subject<ShortFilm[]>();

  constructor(@Inject(LOCAL_STORAGE) private localStorage: WebStorageService) { }

  importHistory(): void {
    let filmHistory: ShortFilm[] = JSON.parse(this.localStorage.get('film-history'));
    this.history$.next(filmHistory);
  }

  updateHistory(film: ShortFilm): void {
    let filmHistory: ShortFilm[] = JSON.parse(this.localStorage.get('film-history'));
    let newFilmHistory: ShortFilm[] = [];
    if (filmHistory) {
      let flag = false;
      filmHistory.map((filmEl: ShortFilm) => {
        if (filmEl.imdbID !== film.imdbID) {
          newFilmHistory.push(filmEl);
        } else {
          flag = true;
        }
      });
      if (!flag) {
        newFilmHistory.pop();
      }
    }
    newFilmHistory.unshift(film);
    this.localStorage.set('film-history', JSON.stringify(newFilmHistory));
    this.history$.next(newFilmHistory);

  }

}
