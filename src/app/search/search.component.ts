import { Component, OnInit } from '@angular/core';
import { Subject, Observable, of, from } from 'rxjs';
import { ShortFilm } from '../omdb';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import { OmdbService } from '../omdb.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent implements OnInit {

  public films$: Observable<ShortFilm[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private omdbService: OmdbService,
  ) { }

  ngOnInit() {
    this.films$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => {
        if (term.length >= 5) {
          return this.omdbService.searchFilms(term, 1);
        } else {
          return of([]);
        }
      }),
    );
  }

  search(term: string): void {
    this.searchTerms.next(term.trim());
  }

}
