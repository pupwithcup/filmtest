import { Component, OnInit } from '@angular/core';
import { LocalService } from '../local-storage.service';
import { Observable, Subject, of } from 'rxjs';
import { ShortFilm } from '../omdb';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.less']
})
export class HistoryComponent implements OnInit {

  public films: ShortFilm[];

  constructor(private localService: LocalService) { }

  ngOnInit() {
    this.localService.history$.subscribe((films: ShortFilm[]) => {
      this.films = films;
      console.log(films);
    });
    this.localService.importHistory();
  }

}
