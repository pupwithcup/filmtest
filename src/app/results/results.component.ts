import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ShortFilm } from '../omdb';
import { OmdbService } from '../omdb.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.less']
})
export class ResultsComponent implements OnInit {

  public films$: Observable<ShortFilm[]>;

  constructor(
    private omdbService: OmdbService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.films$ = this.route.params.pipe(switchMap(routeParams => {
      return this.omdbService.searchFilms(routeParams.term, 2);
    }));
  }

}
