import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResultsComponent } from './results/results.component';
import { FilmDetailComponent } from './film-detail/film-detail.component';


const routes: Routes = [
  { path: '', redirectTo: '/search-results/', pathMatch: 'full' },
  { path: 'search-results/:term', component: ResultsComponent },
  { path: 'film/:imdbID', component: FilmDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
